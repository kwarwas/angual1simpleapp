﻿(function () {
    angular
        .module("ath")
        .controller("pizzaController", ["pizzaService", pizzaController]);

    function pizzaController(pizzaService) {
        var vm = this;
        vm.data = [];

        pizzaService.fetchPizza()
            .then(function (response) {
                vm.data = response.data;
                console.log(response.data);
            });
    }
})();
