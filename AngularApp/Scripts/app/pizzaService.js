﻿(function () {
    angular
        .module("ath")
        .factory("pizzaService", ["$http", "$q", pizzaServiceFactory]);

    function pizzaServiceFactory($http, $q) {
        return {
            fetchPizza: fetchPizza,
        };

        function fetchPizza() {
            return $http
                .get("Home/GetData")
                .catch(function (err) {
                    alert("something went wrong");
                    return $q.reject(err);
                });
        }
    }
})();