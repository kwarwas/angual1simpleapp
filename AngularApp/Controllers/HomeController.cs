﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication8.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetData()
        {
            return Json(new[] 
            {
                new { Id = 1, Name = "Margharita", Description = "ser, szynka, pieczarki, cebula", Type = "Pizza", ImageUrl = "img/pizza1.jpg" },
                new { Id = 2, Name = "Favorita", Description = "ser, szynka, pomidor, kukurydza", Type = "Pizza", ImageUrl = "img/pizza2.jpg" },
                new { Id = 3, Name = "Bosmańska", Description = "ser, kukurydza, tuńczyk", Type = "Pizza", ImageUrl = "img/pizza3.jpg" },
                new { Id = 4, Name = "Wegetariańska", Description = "ser, pieczarki, kukurydza, groszek, papryka", Type = "Pizza", ImageUrl = "img/pizza4.jpg" },
                new { Id = 5, Name = "Pikantna", Description = "ser, pieczarki, cebula, groszek, papryka, kiełbasa", Type = "Pizza", ImageUrl = "img/pizza5.jpg" },
                new { Id = 6, Name = "Tropikalna", Description = "ser, szynka, ananas, brzoskwinia, banan", Type = "Pizza", ImageUrl = "img/pizza6.jpg" },
                new { Id = 7, Name = "Wiejska", Description = "ser, kukurydza, kiełbasa, boczek", Type = "Pizza", ImageUrl = "img/pizza7.jpg" },
                new { Id = 8, Name = "Quatro", Description = "cztery rodzaje sera", Type = "Pizza", ImageUrl = "img/pizza8.jpg" },
                new { Id = 9, Name = "Salami", Description = "ser, pieczarki, salami", Type = "Pizza", ImageUrl = "img/pizza9.jpg" },
                new { Id = 10, Name = "Frutti Di Mare", Description = "ser, tuńczyk, owoce morza, oliwki", Type = "Pizza", ImageUrl = "img/pizza10.jpg" },
                new { Id = 11, Name = "Connpollo", Description = "ser, cebula, kukurydza, papryka, kurczak", Type = "Pizza", ImageUrl = "img/pizza11.jpg" },
                new { Id = 12, Name = "Lasagna", Description = "lasagna mięsna", Type = "Makarony", ImageUrl = "img/pasta1.jpg" },
                new { Id = 13, Name = "Fetuccine Con Pollo", Description = "fetuccine z kurczakiem w sosie kremowo - pomidorowym", Type = "Makarony", ImageUrl = "img/lasagne1.jpg" },
                new { Id = 14, Name = "Pepsi / 7up / Mirinda", Description = "0.3l", Type = "Napoje", ImageUrl = "img/drink1.jpg" },
                new { Id = 16, Name = "Lipton Ice Tea 0,5L", Description = "Lemon Tea, Peach Tea, Green Tea, Green Tea Mango, Red Tea", Type = "Napoje", ImageUrl = "img/drink2.jpg" },
                new { Id = 15, Name = "Woda mineralna", Description = "gazowana lub niegazowana 0.5l", Type = "Napoje", ImageUrl = "img/drink3.jpg" }
            }, JsonRequestBehavior.AllowGet);
        }
    }
}